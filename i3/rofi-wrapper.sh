#!/bin/bash

# When running this file, use 'run' or 'window' as the argument to execute the one-off
# Example: ./path/to/script run

# At some point, add the ability to launch custom scripts for convenience functions
  # - Running Arandr scripts
  # - Saving session
  # - Cleandocker

rofi \
-show $1  
