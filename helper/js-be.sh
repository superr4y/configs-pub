#!/bin/sh
for js in $(find -iname "*.js"); do
	fn=$(echo $js | sed 's/\.js//')
	/usr/local/bin/js-beautify -o "${fn}_beautify.js" $js 
done

