#!/bin/bash

# i3
ln -s "$(pwd)"/i3 ~/.i3
ln -s $(pwd)/i3/rofi-wrapper.sh ~/bin/rofi-wrapper.sh
ln -s "$(pwd)"/i3/Xresources ~/.Xresources

# tmux
ln -s "$(pwd)"/tmux/tmux.conf ~/.tmux.conf

# zsh
ln -s "$(pwd)"/zsh/zshrc ~/.zshrc
mkdir -p ~/.zsh/cache

# fixes
mkdir ~/bin
ln -s $(pwd)/fixes/wakeup_fix.sh ~/bin/wakeup_fix.sh
ln -s $(pwd)/fixes/brightness.sh ~/bin/brightness.sh

# htop
mkdir ~/.config/htop
ln -s $(pwd)/htop/htoprc ~/.config/htop/htoprc

# nvim
mkdir -p ~/.config/nvim
mkdir -p ~/.local/share/nvim/site/autoload/
ln -s $(pwd)/nvim/plug.vim ~/.local/share/nvim/site/autoload/plug.vim
ln -s $(pwd)/nvim/init.vim ~/.config/nvim/init.vim
ln -s $(pwd)/nvim/init.vim ~/.nvimrc
